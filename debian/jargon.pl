#!/usr/bin/perl
#
# Look something up in the jargon file
#

if ( $#ARGV > -1 )
{ 
    if ( $ARGV[0] eq '-s' )
    {
        $stdout = 1; shift;
        if ( $#ARGV <= -1 )
        {
            print "jargon: entryname is mandatory if -s specified\n";
            exit 1;
        }
    }
    else
    {
        $stdout = 0;
    }
    
    $entry = join " ", @ARGV ;
    
    if ( $entry =~ /^([a-zA-z])/ )
    {
        $index = "= $1 =" ;
    }
    else
    {
        $index = '= [^A-Za-z] =' ;
    }

    if ($stdout)
    {
        close STDERR;  # Don't print progress messages
        exec "info", "-o", "-", "jargon", "The Jargon Lexicon", "$index", "$entry";
    }
    else
    {
        exec "infobrowser", "jargon", "The Jargon Lexicon", "$index", "$entry";
    }
}
else
{
    exec "infobrowser", "jargon";
}

